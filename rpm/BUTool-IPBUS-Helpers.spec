#
# spefile for BUTool-IPBUS-Helpers library
#
Name: %{name} 
Version: %{version} 
Release: %{release} 
Packager: %{packager}
Summary: BUTool-IPBUS-Helpers
License: Apache License
Group: BUTool-IPBUS-Helpers
Source: https://gitlab.com/BUTool/BUTool-IPBUS-Helpers
URL: https://gitlab.com/BUTool/BUTool-IPBUS-Helpers
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-buildroot 
Prefix: %{_prefix}

%description
BUTool IPBUS helper wrappers

%prep

%build

%install 

# copy includes to RPM_BUILD_ROOT and set aliases
mkdir -p $RPM_BUILD_ROOT%{_prefix}
cp -rp %{sources_dir}/* $RPM_BUILD_ROOT%{_prefix}/.

#Change access rights
chmod -R 755 $RPM_BUILD_ROOT%{_prefix}/lib

%clean 

%post 

%postun 

%files 
%defattr(-, root, root) 
%{_prefix}/lib/*
%{_prefix}/include/*


